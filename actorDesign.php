<?php
//Comprueba si la sesión está empezada.
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>


<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, shrink-to-fit=no">
        <title>Serie+ | Juego de Tronos</title>  
        <link href="css/gestor_1.css" rel="stylesheet" type="text/css"/>

    </head>    

    <body>
        <div class="row">
            <div class="col-2">
                <!----------------------------------Banner de las series---------------------------------------------------------->
                <div class="row right">
                    <div class="col-12 text-center text-primary">
                        <br><img class="img-fluid" src="img/actores/emilia.jpg">
                    </div>
                    <div class="col-12 text-center  font-weight-bold" style="color: black">
                        <br><h4 class="font-weight-bold" style="color: #4484CE">Emilia Isobel Euphemia Rose Clarke<hr width="75%" /></h4>
                    </div>
                    <div class="col-12 text-center">
                        <h5 class="font-weight-bold" style="color: #F9CF00"><u>Información General</u></p></h5>
                    </div>
                    <div class="col-2">

                    </div>
                    <dl>
                        <dt><b>Sexo:</b></dt>
                        <dd>- Mujer</dd>

                        <dt>Nacionalidad:</dt>
                        <dd>- Britanica</dd>

                        <dt><b>Lugar de Nacimiento:</b></dt>
                        <dd>- Londres, Reino Unido</dd>

                        <dt>Fecha de nacimiento:</dt>
                        <dd>- 23 de octubre de 1986</dd>

                        <dt>Edad:</dt>
                        <dd>- 32 años</dd>
                    </dl> 
                    <hr width="75%" />
                </div>
            </div>
            <!------------------------------------------------------------------------------------------------------------------------>

            <!----------------------------------------------Info de las series-------------------------------------------------------->
            <div class="col-10 border-left border-warning left">
                <br>
                <div class="row">
                    <div class="col-2 text-center border-right">
                        <br>
                        <div class="col-11 text-center">
                            <h5><b class="text-primary">Puntuación :</b></h5>
                        </div>
                        <div class="col-11 text-center">                            
                            <h6><b>4 / 5</b></h6>
                        </div>

                        <div class="rate col-10 text-center img-fluid">
                            <input type="radio" id="star5" name="rate" value="5" />
                            <label for="star5" title="5 Estrellas">5 estrellas</label>
                            <input type="radio" id="star4" name="rate" value="4" />
                            <label for="star4" title="4 Estrellas">4 estrellas</label>
                            <input type="radio" id="star3" name="rate" value="3" />
                            <label for="star3" title="3 Estrellas">3 estrellas</label>
                            <input type="radio" id="star2" name="rate" value="2" />
                            <label for="star2" title="2 Estrellas">2 estrellas</label>
                            <input type="radio" id="star1" name="rate" value="1" />
                            <label for="star1" title="1 Estrellas">1 estrellas</label>                           
                        </div>
                    </div>
                    <div class="col-10">
                        <br> 
                        <div class="row">
                            <div class="col-4"><h5><b class="text-primary">Trending :</b> #3</h5></div>
                            <div class="col-4"><h5><b class="text-primary">Seguidores :</b> 150</h5></div>
                            <div class="col-4">
                                <h5><b class="text-primary">Redes sociales :</b> 
                                    <br><a href="#"><i class="fab fa-twitter"></i></a>&nbsp;
                                    <a href="#"><i class="fab fa-instagram"></i></a>&nbsp;
                                </h5>
                            </div>
                                    
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-4">
                                <button id="lista" type="button" class="btn btn-secondary text-center">Seguir actor <img src="img/suma(1).png" alt=""/></button>
                            </div>
                        </div>
                    </div>
                    <br>
                    <hr style="width: 98%"></hr>

                    <!--------------------------------------------Descripcion de la serie------------------------------------------->
                    <br>
                    <br>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12">
                                <h5 class="font-weight-bold text-primary" ><b>Breve biografía:</b></h5>
                            </div>
                            <div class="col-12">
                                <h5 class="text-justify">
                                    Nació y creció en Berkshire junto a sus padres y su hermano mayor. Su padre era ingeniero 
                                    de audio de teatro. El interés de Clarke en la interpretación comenzó a la edad de tres años después 
                                    de ver una adaptación del musical Show Boat, donde su padre estaba trabajando en ese momento. Se educó 
                                    en la Rye St Antony School y en la St Edward's School en Oxford. Luego asistió a la London Academy of 
                                    Music and Dramatic Art , donde se graduó en 2009.
                                    <br><br>
                                    Tras graduarse, Clarke comenzó su carrera con un papel en la serie de la BBC Doctors, y un papel co-protagonista en el telefilme norteamericano Triassic Attack.
                                    Desde 2011 ha interpretado a Daenerys Targaryen en la serie fantástica y de gran éxito de HBO Juego de tronos, basada en la serie de novelas Canción de 
                                    hielo y fuego, escritas por George R. R. Martin.Por su papel en la serie, fue nominada tres veces a los Premios Primetime Emmy en la categoría de mejor actriz 
                                    de reparto en Serie dramática. 
                                    <br><br>
                                    En 2012 participó en las películas Spike Island en el papel de Sally Harris, y en el cortometraje de suspense Shackled como Malu.
                                    En 2013 realizó su debut en Broadway interpretando a Holly Golightly en la adaptación teatral del clásico de Truman Capote de 1958 Breakfast at Tiffany's. 
                                    Además de poner voz a Marianne en la serie animada Futurama en el episodio "Stench and Stenchibility". En la gran pantalla, Clarke protagonizó junto con Jude Law 
                                    y Demián Bichir la película Dom Hemingway que fue estrenada por Fox Searchlight Pictures el 4 de abril de 2014. 
                                    <br><br>
                                    En 2015 protagonizó junto con Arnold Schwarzenegger 
                                    la película Terminator Génesis interpretando a Sarah Connor. Se le ofreció el papel de Anastasia Steele en la película Cincuenta sombras de Grey pero lo rechazó 
                                    debido a la gran cantidad de desnudos que debía hacer. 
                                    <br><br>
                                    En 2016 interpretó a Louisa "Lou" Clark en la película Me Before You basada en el libro homónimo escrito por 
                                    Jojo Moyes. Luego participó en Above Suspicion en el papel de Susan Smith, estrenada en 2017. En noviembre de 2016, fue confirmada para interpretar a Qi'Ra en el 
                                    spin-off de Star Wars Han Solo: una historia de Star Wars, junto a los actores Alden Ehrenreich y Donald Glover. Fue confirmada para un papel en la película The 
                                    Beauty Inside, a estrenarse en 2018.
                                </h5>
                            </div>
                        </div>
                    </div>
                    <hr style="width: 98%"></hr>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-6">
                                <h5 class="font-weight-bold text-primary" ><b>Participación en series:</b></h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <table class="table table-striped table-responsive-xl table-bordered text-center">
                                    <thead>
                                        <tr>
                                            <th scope="col">Portada</th>
                                            <th scope="col">Serie</th>
                                            <th scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row"><img src="img/portadas3/got.png"></th>
                                            <td class="align-middle">Juego de Tronos</td>
                                            <td class="align-middle"><a id="serie" href="#">ver más</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

<script>
    $('#serie').click(function () {
        $('#principal').load('serieDesign.php');
        console.log('lanzo serieDesign');
    });
</script> 