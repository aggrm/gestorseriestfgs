-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-01-2019 a las 18:58:11
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 7.0.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `gestorseries`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actor`
--

CREATE TABLE `actor` (
  `id_actor` int(11) NOT NULL,
  `Nombre` varchar(100) DEFAULT '',
  `Apellido` varchar(100) DEFAULT '',
  `Genero` varchar(2) DEFAULT '',
  `Lugar_Nacimiento` varchar(100) DEFAULT '',
  `Pais` varchar(50) DEFAULT '',
  `Fecha_nacimiento` date DEFAULT NULL,
  `img` varchar(50) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `actor`
--

INSERT INTO `actor` (`id_actor`, `Nombre`, `Apellido`, `Genero`, `Lugar_Nacimiento`, `Pais`, `Fecha_nacimiento`, `img`) VALUES
(1, 'Úrsula', 'Corberó', 'F', 'Sant Pere de Vilamajor', 'España', '1986-08-11', 'ursula.png'),
(2, 'Álvaro Antonio', 'García', 'M', 'Algeciras, Cádiz', 'España', '1975-02-23', 'elProfesor.png'),
(3, 'Miguel', 'Herrán', 'M', 'Málaga, Andalucía', 'España', '1996-04-25', 'miguelHerrán.png'),
(4, 'Jaime ', 'Lorente López', 'M', 'Murcia', 'España', '1991-12-12', 'jaimeLorente.png'),
(5, 'María', 'Pedraza', 'F', 'Madrid', 'España', '1996-01-26', 'mariaPedraza'),
(6, 'Paco', 'Tous', 'M', 'El puerto de Santa María, Cádiz', 'España', '1964-02-10', 'pacoTous.png'),
(7, 'Esther', 'Acebo', 'F', 'Madrid', 'España', '1983-01-19', 'estherAcebo.png'),
(8, 'Enrique\r\n', 'Arce', 'M', 'Valencia', 'España', '1972-10-08', 'enriqueArce.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `capitulo`
--

CREATE TABLE `capitulo` (
  `id_capitulo` int(11) NOT NULL,
  `Nombre` varchar(100) DEFAULT '',
  `Sinopsis` varchar(500) DEFAULT '',
  `Duracion_cap` varchar(10) DEFAULT '0:00:00',
  `img` varchar(50) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productora`
--

CREATE TABLE `productora` (
  `id_productora` int(11) NOT NULL,
  `Nombre` varchar(100) DEFAULT '',
  `Num_usuarios_fav` int(50) DEFAULT '0',
  `Biografia` varchar(500) DEFAULT '',
  `Logo_img` varchar(50) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `productora`
--

INSERT INTO `productora` (`id_productora`, `Nombre`, `Num_usuarios_fav`, `Biografia`, `Logo_img`) VALUES
(1, 'HBO', 0, 'Es un canal de televisión por suscripción estadounidense, propiedad de la empresa WarnerMedia con sede ubicada en Nueva York. Tras la compra de Time Warner por AT&T esta paso a control de la misma. Su programación está basada en el estreno de películas ya exhibidas en cine y en películas y series de producción propia, entre las que destacan Los Soprano, The Wire, Band of Brothers, The Pacific, Sex and the City, True Blood y Game of Thrones.', 'hbo.png'),
(2, 'Robert Kirkman', 0, 'Es un guionista de cómics estadounidense conocido por su trabajo en The Walking Dead, Outcast​, e Invincible para Image Comics, así como Ultimate X-Men y Marvel Zombies de Marvel Comics. También ha colaborado con el co-fundador de Image Comics, Todd McFarlane en la serie Haunt.', 'null'),
(3, 'Frank Darabont', 0, 'Es un director de cine, productor y guionista estadounidense de origen húngaro.Darabont rodó su primer largometraje en 1990. Su exito llego con las adaptación de una película, la llamó The Shawshank Redemption y Darabont se encargó de adaptar el guion y de dirigirla. Por esta película Darabont fue candidato al Óscar al mejor guion adaptado, además de conseguir otra nominación en la categoría de mejor película. Sin embargo, no consiguió la nominación en la categoría de mejor dirección.', 'null'),
(4, 'AtresMedia', 0, 'Es un grupo de comunicación español que opera en varios sectores de actividad, especialmente la audiovisual.2​Es la sociedad resultante de la fusión del Grupo Antena 3 con la Gestora de Inversiones Audiovisuales La Sexta. Atresmedia Corporación tiene, como empresa madre, la filial Atresmedia Televisión, que obtuvo en el año 1989 una de las tres licencias para televisión privada de España.', 'atresMedia.png'),
(5, 'ABC Studios', 0, 'Es una de las cuatro cadenas de televisión de radiodifusión comercial más grandes en los Estados Unidos. Creada en 1943 a partir de la red anterior NBC Blue Network, la ABC ahora es propiedad de The Walt Disney Company y es parte del Disney–ABC Television Group. La primera emisión de esta cadena en la televisión fue en 1948. Como una de las \"Big Three Television Networks\" (Tres Grandes Cadenas de Televisión), la cadena ha contribuido a la cultura popular de Estados Unidos con su programación.', 'abc.png'),
(6, 'The History Channel', 0, ' es un canal de televisión por suscripción propiedad de A&E Networks. El canal, en sus primeros años, emitía documentales y series de ficción de género histórico como parte de su programación. Actualmente, ha emitido programación no relacionada con la historia; como por ejemplo, series como Pawn Stars, Ax Men, programas de telerrealidad. ', 'THC.png'),
(7, 'Zeta Producciones', 0, 'Es española y ha llegado a realizar dos series como Élite y examen de Conciencia. Ambas son para la plataforma virtual Netfilx. Ademas Zeta tiene un programa que se llama Act ¡Usted perdone!.', 'zetaP.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `serie`
--

CREATE TABLE `serie` (
  `id_serie` int(11) NOT NULL,
  `Nombre` varchar(100) DEFAULT '',
  `Numero_capitulos` int(100) DEFAULT '0',
  `Numero_temporadas` int(2) DEFAULT '0',
  `Duracion_media_capitulo` int(2) DEFAULT '0',
  `Estado_Serie` int(2) DEFAULT '0',
  `Genero` varchar(100) DEFAULT '',
  `Sinopsis` varchar(500) DEFAULT '',
  `Estrellas` int(1) DEFAULT '0',
  `Portada_img` varchar(50) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `serie`
--

INSERT INTO `serie` (`id_serie`, `Nombre`, `Numero_capitulos`, `Numero_temporadas`, `Duracion_media_capitulo`, `Estado_Serie`, `Genero`, `Sinopsis`, `Estrellas`, `Portada_img`) VALUES
(1, 'Juego de Tronos', 68, 8, 68, 3, 'Fantasía', 'Es una serie estadounidense de fantasía medieval, drama y aventuras. Está basada en la serie de novelas Canción de hielo y fuego. La serie está ambientada en los continentes ficticios de Poniente y Essos, y recorre distintos arcos narrativos en los cuales participan un gran número de personajes.', 4, 'got.png'),
(2, 'The Walking Dead', 123, 9, 50, 3, 'Drama Televisivo, Supervivencia Apocalíptica ', 'Es una serie basada en el cómic “The walking Dead”. Esta se sitúa en un mundo postapocalíptico. La civilización ha desaparecido debido a un inexplicable fenómeno que hace que las personas muertas se pongan en pie y ataquen a las personas vivas, transformándolas a su vez en \"caminantes muertos\". Los grupos de supervivientes tendrán que buscar la manera de sobrevivir a este mundo devastado', 3, 'twd.png'),
(3, 'Lost', 121, 6, 58, 1, 'Frama sobrenatural, Ciencia ficción, Suspense', 'Es una serie estadounidense. Esta narra las vivencias de los pasajeros supervivientes del vuelo 815 de Oceanic Airlines Sydney- Los Ángeles en una isla aparentemente desierta, en la cual ocurren cosas muy extrañas. La relación pasajeros isla darán sentido a sus vidas.', 3, 'Lost.png'),
(4, 'Vikingos', 66, 5, 45, 3, 'Drama historico', 'Es una serie canadiense e irlandesa. La serie está basada en los relatos semilegendarios de Ragnar Lodbrok, reconocido como uno de los primeros reyes de Suecia y Dinamarca, durante el siglo VIII. El vikingo Ragnar Lodbrok, uno de los héroes más famosos de la cultura nórdica, quiere lanzarse a explorar los territorios al oeste de Escandinavia a conseguir nuevas tierras para su tribu nórdica.', 4, 'Vikingos'),
(5, 'Anatomía de Grey', 325, 15, 0, 0, 'Drama Médico', 'Es una serie de televisión estadounidense. Narra el día a día de los cirujanos de un ficticio hospital de Seattle, aunque la serie se rueda en Los Ángeles.', 4, 'Adg.png'),
(6, 'La casa de Papel', 15, 2, 68, 3, 'Suspense, Atraco', 'Es una serie española. Un hombre misterioso, forma un grupo de ocho atracadores con ciertas habilidades que no tienen nada que perder. Ellos ejecutaran el mayor atraco de bancos de la historia, tratando de llevarse 2400 millones de euros.', 3, 'Lcdp.png'),
(7, 'Élite', 8, 1, 50, 3, 'Drama, Suspense', 'Es una serie española. sigue la vida de tres jóvenes que han recibido una beca para estudiar en Las Encinas, un instituto prestigioso de España, tras el derrumbe que sufrió su anterior instituto. La llegada de Samuel, Nadia y Christian no será nada fácil, pues los estudiantes que se encuentran en dicho instituto no les harán la vida sencilla. Pero entre la humillación y el acoso escolar ocurre el asesinato de uno de los adolescentes, y sólo quedará por descubrir quién fue el asesino.', 3, 'Elite.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `serie_actor`
--

CREATE TABLE `serie_actor` (
  `id_serie` int(11) NOT NULL,
  `id_actor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `serie_actor`
--

INSERT INTO `serie_actor` (`id_serie`, `id_actor`) VALUES
(6, 1),
(6, 2),
(6, 4),
(6, 5),
(6, 6),
(6, 7),
(6, 8),
(7, 4),
(7, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `serie_productora`
--

CREATE TABLE `serie_productora` (
  `id_serie` int(11) NOT NULL,
  `id_productora` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `serie_productora`
--

INSERT INTO `serie_productora` (`id_serie`, `id_productora`) VALUES
(1, 1),
(2, 2),
(2, 3),
(3, 5),
(4, 6),
(5, 5),
(6, 4),
(7, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `serie_usuario`
--

CREATE TABLE `serie_usuario` (
  `id_serie` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `serie_usuario`
--

INSERT INTO `serie_usuario` (`id_serie`, `id_user`) VALUES
(1, 1),
(1, 2),
(6, 2),
(7, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_user` int(11) NOT NULL,
  `Alias` varchar(100) DEFAULT '',
  `Correo` varchar(100) DEFAULT '',
  `Password` varchar(200) DEFAULT '',
  `Nombre` varchar(50) DEFAULT '',
  `Apellido` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_user`, `Alias`, `Correo`, `Password`, `Nombre`, `Apellido`) VALUES
(1, 'a', 'test@gmail.com', '$2y$10$I4XGjLi.k9Jc7bUcp9Ba2.AVYfBdhy8blv5jlGPwbdr', 'Test', 'Test'),
(2, 'aggrm', 'alberto.goujon@gmail.com', '$2y$10$KkAgfarkLl8RvDO4I9N60usRG2ZJsXswi4PIWNowrnA', 'Alberto', 'Goujon Gutierréz');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_actor`
--

CREATE TABLE `usuario_actor` (
  `id_user` int(11) NOT NULL,
  `id_actor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario_actor`
--

INSERT INTO `usuario_actor` (`id_user`, `id_actor`) VALUES
(2, 4),
(2, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_productora`
--

CREATE TABLE `usuario_productora` (
  `id_user` int(11) NOT NULL,
  `id_productora` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario_productora`
--

INSERT INTO `usuario_productora` (`id_user`, `id_productora`) VALUES
(2, 1),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(2, 7);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actor`
--
ALTER TABLE `actor`
  ADD PRIMARY KEY (`id_actor`);

--
-- Indices de la tabla `capitulo`
--
ALTER TABLE `capitulo`
  ADD PRIMARY KEY (`id_capitulo`);

--
-- Indices de la tabla `productora`
--
ALTER TABLE `productora`
  ADD PRIMARY KEY (`id_productora`);

--
-- Indices de la tabla `serie`
--
ALTER TABLE `serie`
  ADD PRIMARY KEY (`id_serie`);

--
-- Indices de la tabla `serie_actor`
--
ALTER TABLE `serie_actor`
  ADD PRIMARY KEY (`id_serie`,`id_actor`),
  ADD KEY `id_actor` (`id_actor`);

--
-- Indices de la tabla `serie_productora`
--
ALTER TABLE `serie_productora`
  ADD PRIMARY KEY (`id_serie`,`id_productora`),
  ADD KEY `id_productora` (`id_productora`);

--
-- Indices de la tabla `serie_usuario`
--
ALTER TABLE `serie_usuario`
  ADD PRIMARY KEY (`id_serie`,`id_user`),
  ADD KEY `id_user` (`id_user`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `Password` (`Password`);

--
-- Indices de la tabla `usuario_actor`
--
ALTER TABLE `usuario_actor`
  ADD PRIMARY KEY (`id_actor`,`id_user`),
  ADD KEY `id_user` (`id_user`);

--
-- Indices de la tabla `usuario_productora`
--
ALTER TABLE `usuario_productora`
  ADD PRIMARY KEY (`id_productora`,`id_user`),
  ADD KEY `id_user` (`id_user`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actor`
--
ALTER TABLE `actor`
  MODIFY `id_actor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4080;

--
-- AUTO_INCREMENT de la tabla `capitulo`
--
ALTER TABLE `capitulo`
  MODIFY `id_capitulo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4080;

--
-- AUTO_INCREMENT de la tabla `productora`
--
ALTER TABLE `productora`
  MODIFY `id_productora` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4084;

--
-- AUTO_INCREMENT de la tabla `serie`
--
ALTER TABLE `serie`
  MODIFY `id_serie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4080;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4082;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `serie_actor`
--
ALTER TABLE `serie_actor`
  ADD CONSTRAINT `serie_actor_ibfk_1` FOREIGN KEY (`id_serie`) REFERENCES `serie` (`id_serie`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `serie_actor_ibfk_2` FOREIGN KEY (`id_actor`) REFERENCES `actor` (`id_actor`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `serie_productora`
--
ALTER TABLE `serie_productora`
  ADD CONSTRAINT `serie_productora_ibfk_1` FOREIGN KEY (`id_serie`) REFERENCES `serie` (`id_serie`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `serie_productora_ibfk_2` FOREIGN KEY (`id_productora`) REFERENCES `productora` (`id_productora`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `serie_usuario`
--
ALTER TABLE `serie_usuario`
  ADD CONSTRAINT `serie_usuario_ibfk_1` FOREIGN KEY (`id_serie`) REFERENCES `serie` (`id_serie`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `serie_usuario_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `usuario` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario_actor`
--
ALTER TABLE `usuario_actor`
  ADD CONSTRAINT `usuario_actor_ibfk_1` FOREIGN KEY (`id_actor`) REFERENCES `actor` (`id_actor`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usuario_actor_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `usuario` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuario_productora`
--
ALTER TABLE `usuario_productora`
  ADD CONSTRAINT `usuario_productora_ibfk_1` FOREIGN KEY (`id_productora`) REFERENCES `productora` (`id_productora`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `usuario_productora_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `usuario` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
