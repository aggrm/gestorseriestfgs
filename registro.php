<?php
include ('misFunciones.php');

function limpiaPalabra($palabra) {
//filtro muy básico para evitar la inyección SQL -> 'OR'1'='1
    $palabra = trim($palabra, "'");
    $palabra = trim($palabra, " ");
    $palabra = trim($palabra, "-");
    $palabra = trim($palabra, "`");
    $palabra = trim($palabra, '"');
    return $palabra;
}

$mysqli = conectaBBDD();
$cajaNombreR = limpiaPalabra($_POST['cajaNombreR']);
$cajaApellidoR = limpiaPalabra($_POST['cajaApellidoR']);
$cajaPasswordR = limpiaPalabra($_POST['cajaPasswordR']);
$cajaCorreoR = limpiaPalabra($_POST['cajaCorreoR']);
$cajaIDR = limpiaPalabra($_POST['cajaIDR']);
$passwordEncriptadaR = password_hash($cajaPasswordR, PASSWORD_BCRYPT);          //Encripto la contraseña

$resultadoQuery = $mysqli->query("INSERT INTO usuario"
        . "(Alias, Correo, Password, Nombre, Apellido)"
        . "VALUES ('$cajaIDR', '$cajaCorreoR', '$passwordEncriptadaR', "
        . "'$cajaNombreR', '$cajaApellidoR' )");                                //El insert de datos de registro

$numUsuarios = $mysqli->affected_rows;

if ($numUsuarios > 0) 
{
    echo ('<div class="alert alert-success" role="alert"> Tu cuenta ha sido correctamente creada.' . $cajaIDR . '</div>');
    echo('<div class="form-group text-center"><a href="index.php" class="ForgetPwd">Aceder al login</a></div>');
} 
else if ($numUsuarios == -1)
{
    echo ('<div class="alert alert-danger" role="alert"> Usuario o correo ya existen</div>');
    echo('<div class="form-group text-center"><a id="accedeRegistro" class="ForgetPwd">Aceder a la creacion de cuenta otra vez</a></div>');
}
?> 

<script>
    $('#accedeRegistro').click(function () {
        $('#principal').load('registroDesign.php');
    });
</script>

