<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, shrink-to-fit=no">
        <title>Series+</title>

        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/> 
        <link href="css/materialize.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" 
              integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link href="css/gestor.css" rel="stylesheet" type="text/css"/>

        <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/materialize.js" type="text/javascript"></script>



    </head>
    <body>
        <div class="container-fluid encabezado"> 
            <!--BARRA DE NAVEGACIÓN-->
            <nav class="row navbar navbar-expand-sm navbar-light stickytop">                
                <a class="col-4" href="index.php"><img id="logo" src="img/logoGrande2.png"></a>
                <div class="col-4 eslogan"><h2 class="font-weight-bold" style="color: #353535">Tu mundo de series a un click</h2></div>                
                <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarMenu">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarMenu">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php">Inicio</a>
                        </li>
                        <li class="nav-item">
                            <a id="botonCuenta" class="nav-link" href="#">Cuenta</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="logout.php">Log out</a>
                        </li>

                    </ul>
                </div>                
            </nav>
        </div>        

        <div class="container-fluid contenido " id="principal">
            <?php
            //Comprueba si la sesión está empezada.
            if (session_status() == PHP_SESSION_NONE) {
                session_start();
            }
            //Si la variable de sesión no existe lanza el javascript del login
            if (!isset($_SESSION['nombreUsuario'])) {
                echo "
                <script type=\"text/javascript\">
                $('#principal').load('loginDesign.php');
                </script>
                ";
            } else {
                echo "
                <script type=\"text/javascript\">
                $('#principal').load('inicioUserDesign.php');
                </script>
                ";
            }
            ?>
            
        </div>
        <br><br><br><br><br><br><br>
        <!--Footer-->
        <footer class="container-fluid text-center foot">
            <div class="copyrights">
                <a class="link" href="#" target="_blank">Contacto</a>
                <a class="link" href="#" target="_blank">Política de privacidad</a>
                <a class="link" href="#" target="_blank">Términos y condiciones</a>
                <br><br>
                <p class="white-txt">Series+ © 2019
                    <br><br>
                    <img class="footimg" src="img/series.png" alt="Logo">
                </p>
            </div>
        </footer>
        <!--Footer-->

    </body>
</html>

<script>
    $('#botonCuenta').click(function () {
        $('#principal').load('perfilDesign.php');
    });
</script> 