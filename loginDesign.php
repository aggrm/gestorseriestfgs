<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="text-center">
                        <h3><i class="fa fa-user fa-4x" style="color: #F19F4D"></i></h3>
                        <h2 class="text-center">Login</h2>
                        <p>Inicia sesión.</p>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user color-blue"></i></span>
                                    <input id="cajaNombreL"  placeholder="Nombre de Usuario" class="form-control"  type="name">                          
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user color-blue"></i></span>
                                    <input id="cajaPasswordL"  placeholder="Contraseña" class="form-control"  type="password">                          
                                </div>
                            </div>                                
                            <input type="hidden" class="hide btn-morado form" name="token" id="token" value="">                                
                                <div class="form-group">
                                    <input id="botonAcceder" class="btn btn-lg btn-block btn-gris" value="Login" type="submit">
                                </div>
                                <div class="form-group" >
                                    <a id="accedeRegistro" href="#" class="ForgetPwd">Crear cuenta</a>
                                </div>
                            </input>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#botonAcceder').click(function () {
        //Leo el contenido de todas las cajas para luego poder jugar con su contenido 
        var _cajaNombreL = $('#cajaNombreL').val();
        var _cajaPasswordL = $('#cajaPasswordL').val();
        $('#principal').load("login.php", {
            cajaNombreL: _cajaNombreL,
            cajaPasswordL: _cajaPasswordL
        });
    });

    $('#accedeRegistro').click(function () {

        $('#principal').load('registroDesign.php');
    });
</script>