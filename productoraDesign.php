<?php
//Comprueba si la sesión está empezada.
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>


<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, shrink-to-fit=no">
        <title>Serie+ | Juego de Tronos</title>  
        <link href="css/gestor_1.css" rel="stylesheet" type="text/css"/>

    </head>    

    <body>
        <div class="row">
            <div class="col-2">
                <!----------------------------------Banner de las series---------------------------------------------------------->
                <div class="row right">
                    <div class="col-12 text-center text-primary">
                        <br><img class="img-fluid" src="img/hbo_logo.png">
                    </div>
                    <div class="col-12 text-center  font-weight-bold" style="color: black">
                        <br><h4 class="font-weight-bold" style="color: #4484CE">HBO (Home Box Office)<hr width="75%" /></h4>
                    </div>
                    <div class="col-12 text-center">
                        <h5 class="font-weight-bold" style="color: #F9CF00"><u>Información General</u></p></h5>
                    </div>
                    <div class="col-2">

                    </div>
                    <dl>
                        <dt><b>Sede:</b></dt>
                        <dd>- Nueva York, Nueva York</dd>

                        <dt>País:</dt>
                        <dd>- Estados Unidos</dd>

                        <dt><b>Fechad de fundación:</b></dt>
                        <dd>- 8 de noviembre de 1972</dd>

                        <dt>Prpietario:</dt>
                        <dd>- WarnerMedia y AT&T</dd>
                    </dl> 
                    <hr width="75%" />
                </div>
            </div>
            <!------------------------------------------------------------------------------------------------------------------------>

            <!----------------------------------------------Info de las series-------------------------------------------------------->
            <div class="col-10 border-left border-warning left">
                <br>
                <div class="row">
                    <div class="col-2 text-center border-right">
                        <br>
                        <div class="col-11 text-center">
                            <h5><b class="text-primary">Puntuación :</b></h5>
                        </div>
                        <div class="col-11 text-center">                            
                            <h6><b>3 / 5</b></h6>
                        </div>

                        <div class="rate col-10 text-center img-fluid">
                            <input type="radio" id="star5" name="rate" value="5" />
                            <label for="star5" title="5 Estrellas">5 estrellas</label>
                            <input type="radio" id="star4" name="rate" value="4" />
                            <label for="star4" title="4 Estrellas">4 estrellas</label>
                            <input type="radio" id="star3" name="rate" value="3" />
                            <label for="star3" title="3 Estrellas">3 estrellas</label>
                            <input type="radio" id="star2" name="rate" value="2" />
                            <label for="star2" title="2 Estrellas">2 estrellas</label>
                            <input type="radio" id="star1" name="rate" value="1" />
                            <label for="star1" title="1 Estrellas">1 estrellas</label>                           
                        </div>
                    </div>
                    <div class="col-10">
                        <br> 
                        <div class="row">
                            <div class="col-4"><h5><b class="text-primary">Seguidores :</b> 150</h5></div>
                            <div class="col-4">
                                <h5><b class="text-primary">Redes sociales :</b> 
                                    <br><a href="#"><i class="fab fa-twitter"></i></a>&nbsp;
                                    <a href="#"><i class="fab fa-instagram"></i></a>&nbsp;
                                    <a href="#"><i class="fab fa-facebook"></i></a>&nbsp;
                                </h5>
                            </div>
                                    
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-4">
                                <button id="lista" type="button" class="btn btn-secondary text-center">Seguir productora <img src="img/suma(1).png" alt=""/></button>
                            </div>
                        </div>
                    </div>
                    <br>
                    <hr style="width: 98%"></hr>

                    <!--------------------------------------------Descripcion de la serie------------------------------------------->
                    <br>
                    <br>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12">
                                <h5 class="font-weight-bold text-primary" ><b>Breve biografía:</b></h5>
                            </div>
                            <div class="col-12">
                                <h5 class="text-justify">
                                    HBO fue el primer canal de cable y satélite creado como un canal de televisión de transmisión no terrestre. En 1965, el visionario y pionero del cable Charles 
                                    Francis Dolan ganó la franquicia para construir un sistema de cable en Bajo Manhattan, Nueva York. El nuevo sistema, llamado Sterling Manhattan Cable por el 
                                    señor Dolan, fue el primer sistema de cable subterráneo urbano de los Estados Unidos. En vez de colgar el cable de los postes telefónicos, y usando antenas de 
                                    microondas para recibir la señal, Sterling puso cable subterráneo bajo las calles de Manhattan, porque la señal era bloqueada por la gran cantidad de edificios 
                                    altos y rascacielos. Time Life Inc., ese mismo año compró el 20 % de la compañía de Dolan.
                                    <br><br>
                                    A principios de 1970, buscando nuevas fuentes de ingresos, el Sr. Dolan creó un Green Channel para que los suscriptores puedan pagar extra por recibir películas 
                                    sin cortes y sin comerciales, así como coberturas deportivas. Para ayudarlo a dirigir su nuevo proyecto, Dolan contrató como su vicepresidente de programación a 
                                    un abogado joven llamado Gerald Levin, quien tenía experiencia en contratación de películas para transmisión en televisión y eventos deportivos. 
                                    <br><br>
                                    Dolan presentó su idea de Green Channel a la gerencia de Time Life, y aunque la distribución por satélite era una posibilidad remota en esa época, él convenció 
                                    a Time Life para respaldarlos, y muy pronto el "The Green Channel" se convirtió en Home Box Office el 8 de noviembre de 1972. HBO empezó a usar las microondas 
                                    para alimentar su programación. El primer programa en transmitir por el canal de pago fue el partido de los New York Rangers contra los Vancouver Canucks, a un 
                                    sistema de canal de cable en Wilkes Barre, Pensilvania.5​ También en esa noche se vio la primera película en HBO: Sometimes a Great Notion de 1971, dirigida y 
                                    protagonizada por Paul Newman con Henry Fonda como coprotagonista.
                                    <br><br>
                                    Sterling Manhattan Cable perdía dinero rápidamente porque la compañía tenía una pequeña base de suscriptores de 20.000 clientes en Manhattan. El socio de Dolan, 
                                    Time Life Inc., ganó el control del 80 % de Sterling y decidió tomar el control en la operación de Sterling Manhattan. Time Life desechó el nombre Sterling para 
                                    convertirse en Manhattan Cable Television, ganando el control de HBO en marzo de 1973. Gerald Levin reemplazó a Dolan como presidente y director ejecutivo 
                                    (Chief Executive Officer) de HBO. En septiembre de 1973, Time Life Inc. completó su adquisición del servicio de pago. HBO estuvo pronto en 14 sistemas de Nueva 
                                    York y Pensilvania, pero la tasa de rotación era excepcionalmente alta. 
                                    <br><br>
                                    Los suscriptores solicitaban el servicio en prueba por algunas semanas, se cansaban de ver las mismas películas y cancelaban. HBO estaba luchando y algo se tenía 
                                    que hacer. Cuando HBO llegó a Lawrence, Massachusetts, la idea era que los suscriptores pudieran experimentar el servicio previamente y de forma gratuita en el 
                                    canal 3. Después de un mes, el servicio se movió al canal 6 y se codificó. Esta prueba se volvió popular, ganando muchos suscriptores y el concepto se extendió. 
                                </h5>
                            </div>
                        </div>
                    </div>
                    <hr style="width: 98%"></hr>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-6">
                                <h5 class="font-weight-bold text-primary" ><b>Series:</b></h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <table class="table table-striped table-responsive-xl table-bordered text-center">
                                    <thead>
                                        <tr>
                                            <th scope="col">Portada</th>
                                            <th scope="col">Serie</th>
                                            <th scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row"><img src="img/portadas3/got.png"></th>
                                            <td class="align-middle">Juego de Tronos</td>
                                            <td class="align-middle"><a id="serie" href="#">ver más</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

<script>
    $('#serie').click(function () {
        $('#principal').load('serieDesign.php');
        console.log('lanzo serieDesign');
    });
</script> 