<?php
//Comprueba si la sesión está empezada.
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>


<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, shrink-to-fit=no">
        <title>Serie+ | Juego de Tronos</title>  
        <link href="css/gestor_1.css" rel="stylesheet" type="text/css"/>

    </head>    

    <body>
        <div class="row">
            <div class="col-2">
                <!----------------------------------Banner de las series---------------------------------------------------------->
                <div class="row right">
                    <div class="col-12 text-center text-primary">
                        <br><img class="img-fluid" src="img/portadas2/goty.jpg">
                    </div>
                    <div class="col-12 text-center  font-weight-bold" style="color: black">
                        <br><h4 class="font-weight-bold" style="color: #4484CE">Juego de Tronos<hr width="75%" /></h4>
                    </div>
                    <div class="col-12 text-center">
                        <h5 class="font-weight-bold" style="color: #F9CF00"><u>Información General</u></p></h5>
                    </div>
                    <div class="col-2">

                    </div>
                    <dl>
                        <dt><b>Género:</b></dt>
                        <dd>- Fantasía</dd>

                        <dt>Numero de Temporadas:</dt>
                        <dd>- 8 temporadas</dd>

                        <dt><b>Numero de capítulos:</b></dt>
                        <dd>- 69</dd>

                        <dt>Duración mendia de capítulos:</dt>
                        <dd>- 68 minutos</dd>

                        <dt>Estado de la serie:</dt>
                        <dd>- En emisión</dd>

                        <dt>Rating:</dt>
                        <dd>- +18 años</dd>
                    </dl> 
                    <hr width="75%" />
                </div>
            </div>
            <!------------------------------------------------------------------------------------------------------------------------>

            <!----------------------------------------------Info de las series-------------------------------------------------------->
            <div class="col-10 border-left border-warning left">
                <br>
                <div class="row">
                    <div class="col-2 text-center border-right">
                        <br>
                        <div class="col-11 text-center">
                            <h5><b class="text-primary">Puntuación :</b></h5>
                        </div>
                        <div class="col-11 text-center">                            
                            <h6><b>4 / 5</b></h6>
                        </div>

                        <div class="rate col-10 text-center img-fluid">
                            <input type="radio" id="star5" name="rate" value="5" />
                            <label for="star5" title="5 Estrellas">5 estrellas</label>
                            <input type="radio" id="star4" name="rate" value="4" />
                            <label for="star4" title="4 Estrellas">4 estrellas</label>
                            <input type="radio" id="star3" name="rate" value="3" />
                            <label for="star3" title="3 Estrellas">3 estrellas</label>
                            <input type="radio" id="star2" name="rate" value="2" />
                            <label for="star2" title="2 Estrellas">2 estrellas</label>
                            <input type="radio" id="star1" name="rate" value="1" />
                            <label for="star1" title="1 Estrellas">1 estrellas</label>                           
                        </div>
                    </div>
                    <div class="col-10">
                        <br> 
                        <div class="row">
                            <div class="col-4"><h5><b class="text-primary">Ranking :</b> #1</h5></div>
                            <div class="col-4"><h5><b class="text-primary">Trending :</b> #1</h5></div>
                            <div class="col-4"><h5><b class="text-primary">Seguidores :</b> 258</h5></div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-4">
                                <button id="lista" type="button" class="btn btn-secondary">Añadir a una lista <img src="img/suma(1).png" alt=""/></button>
                            </div>
                            <div class="col-4">
                                <button id="lista" type="button" class="btn btn-secondary text-center">Seguir serie <img src="img/suma(1).png" alt=""/></button>
                            </div>
                        </div>
                    </div>
                    <br>
                    <hr style="width: 98%"></hr>

                    <!--------------------------------------------Descripcion de la serie------------------------------------------->
                    <br>
                    <br>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12">
                                <h5 class="font-weight-bold text-primary" ><b>Sinopsis de la serie:</b></h5>
                            </div>
                            <div class="col-12">
                                <h5>Es una serie estadounidense de fantasía medieval, drama y aventuras. 
                                    Está basada en la serie de novelas Canción de hielo y fuego. La serie está ambientada 
                                    en los continentes ficticios de Poniente y Essos, y recorre distintos arcos narrativos, 
                                    participando un gran número de personajes.</h5>
                            </div>
                        </div>
                    </div>
                    <hr style="width: 98%"></hr>

                    <!----------------------------------------------Tabla de actores--------------------------------------------->
                    <br>
                    <br>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-6">
                                <h5 class="font-weight-bold text-primary" ><b>Personajes de la serie :</b></h5>
                            </div>
                            <div class="col-6">
                                <h5 class="font-weight-bold text-primary" ><b>Actor que representa al personaje :</b></h5>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-12">
                                <table class="table table-striped table-responsive-xl text-center table-bordered">
                                    <thead>
                                        <tr>
                                            <th scope="col">Personaje</th>
                                            <th scope="col">Nombre del Personaje</th>
                                            <th scope="col">Rol del personaje</th>
                                            <th scope="col">Actor</th>
                                            <th scope="col">Nombre del actor</th>
                                            <th scope="col">Nacionalidad</th>
                                            <th scope="col"></th>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row"><img src="img/personajesGOT/sansa.jpg"></th>
                                            <td class="align-middle">Sansa Stark</td>
                                            <td class="align-middle">Principal</td>
                                            <th><img src="img/personajesGOT/sophie.jpg"></th>
                                            <td class="align-middle">Sophie Turner</td>
                                            <td class="align-middle">Británica</td>
                                            <td class="align-middle"><a href="#">ver más</a></td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><img src="img/personajesGOT/arya.jpg"></th>
                                            <td class="align-middle">Arya Stark</td>
                                            <td class="align-middle">Principal</td>
                                            <th><img src="img/personajesGOT/maisie.jpg"></th>
                                            <td class="align-middle">Maisie Williams</td>
                                            <td class="align-middle">Británica</td>
                                            <td class="align-middle"><a a id="masie" href="#">ver más</a></td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><img src="img/personajesGOT/brandon.png"></th>
                                            <td class="align-middle">Brandon Stark</td>
                                            <td class="align-middle">Principal</td>
                                            <th><img src="img/personajesGOT/isaac.jpg"></th>
                                            <td class="align-middle">Isaac Hempstead-Wright</td>
                                            <td class="align-middle">Británico</td>
                                            <td class="align-middle"><a href="#">ver más</a></td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><img src="img/personajesGOT/jon.png"></th>
                                            <td class="align-middle">Jon Nieve</td>
                                            <td class="align-middle">Principal</td>
                                            <th><img src="img/personajesGOT/kit.jpg"></th>
                                            <td class="align-middle">Kit Harington</td>
                                            <td class="align-middle">Británico</td>
                                            <td class="align-middle"><a id="jon" href="#">ver más</a></td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><img src="img/personajesGOT/dany.jpg"></th>
                                            <td class="align-middle">Daenerys Targaryen</td>
                                            <td class="align-middle">Principal</td>
                                            <th><img src="img/personajesGOT/emilia.jpg"></th>
                                            <td class="align-middle">Emilia Isobel Euphemia Rose Clarke</td>
                                            <td class="align-middle">Británica</td>
                                            <td class="align-middle"><a a id="emilia" href="#">ver más</a></td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><img src="img/personajesGOT/tyrion.jpg"></th>
                                            <td class="align-middle">Tyrion Lannister</td>
                                            <td class="align-middle">Principal</td>
                                            <th><img src="img/personajesGOT/peter.jpg"></th>
                                            <td class="align-middle">Peter Dinklage</td>
                                            <td class="align-middle">Estadounidense</td>
                                            <td class="align-middle"><a a id="tyrion" href="#">ver más</a></td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><img src="img/personajesGOT/jaime_lannister.jpg"></th>
                                            <td class="align-middle">Jaime Lannister</td>
                                            <td class="align-middle">Principal</td>
                                            <th><img src="img/personajesGOT/nikolaj.jpg"></th>
                                            <td class="align-middle">Nikolaj Coster-Waldau</td>
                                            <td class="align-middle">Danés</td>
                                            <td class="align-middle"><a href="#">ver más</a></td>
                                        </tr>
                                        <tr>
                                            <td colspan="7" class="align-middle text-right" ><a href="#">ver más personajes</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <hr style="width: 98%">

                            <!----------------------------------------------Tabla de productora------------------------------------------->
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-6">
                                        <h5 class="font-weight-bold text-primary" ><b>Productora de la serie :</b></h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <table class="table table-striped table-responsive-xl table-bordered text-center">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Logo</th>
                                                    <th scope="col">Nombre de la Productora</th>
                                                    <th scope="col">Tipo de productora</th>
                                                    <th scope="col">País productora</th>
                                                    <th scope="col">Sede principal</th>
                                                    <th scope="col"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row"><img src="img/hbo-logo.png"></th>
                                                    <td class="align-middle">HBO</td>
                                                    <td class="align-middle">Televisiva</td>
                                                    <td class="align-middle">Estados Unidos</td>
                                                    <td class="align-middle">Nueva York</td>
                                                    <td class="align-middle"><a id="productora" href="#">ver más</a></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <hr style="width: 98%">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-6">
                                        <h5 class="font-weight-bold text-primary" ><b>Temporada de la serie :</b></h5>
                                        <select>
                                            <option value="1">Temporada 1</option>
                                            <option value="2">Temporada 2</option>
                                            <option value="3">Temporada 3</option>
                                            <option value="4">Temporada 4</option>
                                            <option value="4">Temporada 5</option>
                                            <option value="4">Temporada 6</option>
                                            <option value="4" selected>Temporada 7</option>
                                            <option value="4">Temporada 8</option>
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <div class="row">

                                    <div class="col-12">
                                        <table class="table table-striped table-responsive-xl table-bordered text-center">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Capítulo</th>
                                                    <th scope="col">Nombre del capitulo</th>
                                                    <th scope="col">Resumen breve</th>
                                                    <th scope="col">Duración del capítulo</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="align-middle">1</td>
                                                    <td class="align-middle">Rocadragón</td>
                                                    <td class="align-middle">-------------------------------------------------------------------</td>
                                                    <td class="align-middle"> 1 hora y 20 minutos</td>
                                                </tr>
                                                <tr>
                                                    <td class="align-middle">2</td>
                                                    <td class="align-middle">Bajo la tormenta</td>
                                                    <td class="align-middle">-------------------------------------------------------------------</td>
                                                    <td class="align-middle"> 1 hora y 3 minutos</td>
                                                </tr>
                                                <tr>
                                                    <td class="align-middle">3</td>
                                                    <td class="align-middle">La justicia de la reina</td>
                                                    <td class="align-middle">-------------------------------------------------------------------</td>
                                                    <td class="align-middle"> 1 hora y 17 minutos</td>
                                                </tr>
                                                <tr>
                                                    <td class="align-middle">4</td>
                                                    <td class="align-middle">Botines de guerra</td>
                                                    <td class="align-middle">-------------------------------------------------------------------</td>
                                                    <td class="align-middle"> 1 hora y 13 minutos</td>
                                                </tr>
                                                <tr>
                                                    <td class="align-middle">5</td>
                                                    <td class="align-middle">Guardaoriete</td>
                                                    <td class="align-middle">-------------------------------------------------------------------</td>
                                                    <td class="align-middle"> 58 minutos</td>
                                                </tr>
                                                <tr>
                                                    <td class="align-middle">6</td>
                                                    <td class="align-middle">Mas allá del muro</td>
                                                    <td class="align-middle">-------------------------------------------------------------------</td>
                                                    <td class="align-middle"> 1 hora y 10 minutos</td>
                                                </tr>
                                                <tr>
                                                    <td class="align-middle">7</td>
                                                    <td class="align-middle">El dragón y el lobo</td>
                                                    <td class="align-middle">-------------------------------------------------------------------</td>
                                                    <td class="align-middle"> 1 hora y 20 minutos</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!------------------------------------------------------------------------------------------------------------------------>
            </div>
    </body>
</html>

<script>
    $('#emilia').click(function () {
        $('#principal').load('actorDesign.php');
        console.log('lanzo actorDesign 1');
    });
    $('#jon').click(function () {
        $('#principal').load('actorDesign.php');
        console.log('lanzo actorDesign 2');
    });
    $('#masie').click(function () {
        $('#principal').load('actorDesign.php');
        console.log('lanzo actorDesign 3');
    });
    $('#tyrion').click(function () {
        $('#principal').load('actorDesign.php');
        console.log('lanzo actorDesign 4');
    });
    $('#productora').click(function () {
        $('#principal').load('productoraDesign.php');
        console.log('lanzo productoraDesign');
    });
</script> 
