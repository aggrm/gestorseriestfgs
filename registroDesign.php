<?php ?>
<div class="main-login main-center">
    
        
        
        <!-- Este es el grupo de campos del nombre-->
        <div class="form-group">
            <label class="cols-sm-2">Nombre:</label>
            <div class="cols-sm-10">
                <input id="cajaNombreR" class="form-control" type="text" placeholder="Introduce tu nombre"/>
            </div>
        </div>
        
        <!-- Este es el grupo de campos del apellido-->
        <div class="form-group">
            <label class="cols-sm-2">Apellido:</label>
            <div class="cols-sm-10">
                <input id="cajaApellidoR" class="form-control" type="text" placeholder="Introduce tu apellido"/>
            </div>
        </div>

        <!-- Este es el grupo de campos del correo-->
        <div class="form-group">
            <label class="cols-sm-2">Correo Electrónico:</label>
            <div class="cols-sm-10">
                <input id="cajaCorreoR" class="form-control"  type="email" placeholder="Introduce tu email"/>
            </div>
        </div>

        <!-- Este es el grupo de campos del ID-->
        <div class="form-group">
            <label class="cols-sm-2">ID de Usuario:</label>
            <div class="cols-sm-10">
                <input id="cajaIDR" class="form-control" type="text" placeholder="Usuario ID" required="required"/>
            </div>
        </div>

        <!-- Este es el grupo de campos de la contraseña-->
        <div class="form-group">
            <label class="cols-sm-2">Contraseña:</label>
            <div class="cols-sm-10">
                <input id="cajaPasswordR" class="form-control" type="password" placeholder="Contraseña" required="required"/>
            </div>
        </div>

        <!-- Este es el grupo de campos de confirmar contraseña-->
        <div class="form-group">
            <label  class="cols-sm-2">Confirma Contraseña:</label>
            <div class="cols-sm-10">
                <input id="confirmacion_password" class="form-control" type="password" placeholder="Confirma Contraseña"/>
            </div>
        </div>
        
        <!-- Este es el grupo de campos de registro y redireccion al index-->
        <div class="form-group">
            <button id="botonRegistro" class="btn btn-block btn-morado" type="submit">Regístrate</button>
        </div>
        <div class="form-group text-center">
            <a href="index.php" class="ForgetPwd">Login</a>
            </div>
    
</div>


<script>

    $('#botonRegistro').click(function () {
        //Leo el contenido de todas las cajas para luego poder jugar con su contenido 
        var _cajaNombreR = $('#cajaNombreR').val();
        var _cajaApellidoR = $('#cajaApellidoR').val();
        var _cajaCorreoR = $('#cajaCorreoR').val();
        var _cajaIDR = $('#cajaIDR').val();
        var _cajaPasswordR = $('#cajaPasswordR').val();
        var _cajaPassword2R = $('#confirmacion_password').val();

        //Si las cajas no son igual debe de saltar una alerta
        if (_cajaPasswordR !== _cajaPassword2R)
        {
            alert('LA CONTRASEÑA NO COINCIDE');
        } else
        {
            $('#principal').load("registro.php", {
                cajaNombreR: _cajaNombreR,
                cajaApellidoR: _cajaApellidoR,
                cajaCorreoR: _cajaCorreoR,
                cajaIDR: _cajaIDR,
                cajaPasswordR: _cajaPasswordR
            });
        }
    });
</script>

