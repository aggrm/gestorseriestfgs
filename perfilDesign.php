<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-7s5uDGW3AHqw6xtJmNNtr+OBRJUlgkNJEo78P4b0yRw= sha512-nNo+yCHEyn0smMxSswnf/OnX6/KwJuZTlNZBjauKhTK0c+zT+q5JOCx0UFhXQ6rJR9jg6Es8gPuD2uZcYDLqSw==" crossorigin="anonymous">


    <div class="container">
        <div class="row">
            <div class="col-md-10 ">
                <form class="form-horizontal">
                    <fieldset>
                        <!-- Form Name -->
                        <legend>Edita tu perfil</legend>
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="Name (Full name)">Nombre Completo</label>  
                            <div class="col-md-4">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </div>
                                    <input id="cajaNombreP" name="Name (Full name)" type="text" placeholder="Nombre" class="form-control input-md">
                                </div>
                            </div>
                        </div>

                        <!-- File Button -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="Upload photo">Seleccionar Archivo</label>
                            <div class="col-md-4">
                                <input id="Upload photo" name="Selcione un archivo" class="input-file" type="file">
                            </div>
                        </div>
                        
                        <!-- Multiple Radios (inline) -->
                        <div class="form-group">  
                            <label class="col-md-4 control-label" for="Gender">Género</label>    
                            <div class="col-md-4"> 
                                <label class="radio-inline" for="Gender-0">
                                    <input type="radio" name="Gender" id="Gender-0" value="1" checked="checked">Hombre</label> 
                                <label class="radio-inline" for="Gender-1">
                                    <input type="radio" name="Gender" id="Gender-1" value="2">Mujer</label> 
                                <label class="radio-inline" for="Gender-2">
                                    <input type="radio" name="Gender" id="Gender-2" value="3">Otro</label>
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="Email Address">Correo Electrónico</label>  
                            <div class="col-md-4">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-envelope-o"></i>
                                    </div>
                                    <input id="Email Address" name="Email Address" type="text" placeholder="Email Address" class="form-control input-md">
                                </div>
                            </div>
                        </div>

                        <!-- Textarea -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="Overview (max 200 words)">Descripción del perfil</label>
                            <div class="col-md-4">                     
                                <textarea class="form-control" rows="10"  id="Overview (max 200 words)" name="Descripción del perfil">Descripción</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" ></label>  
                            <div class="col-md-4">
                                <a href="#" class="btn btn-success"><span class="glyphicon glyphicon-thumbs-up"></span> Submit</a>
                                <a href="#" class="btn btn-danger" value=""><span class="glyphicon glyphicon-remove-sign"></span> Clear</a>
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
    
    <script src="js/jquery-3.3.1.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
    
    <script type="text/javascript">
        console.log('lanzo perfildesign');
    </script>
</html>