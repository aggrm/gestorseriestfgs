<?php
//Comprueba si la sesión está empezada.
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>

<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, shrink-to-fit=no">
        <title>Series+ | Inicio</title>
    </head>

    <body>
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-2  text-center">
                        <br>
                        <br>
                        <br>
                        <img class="text-center img-fluid" src="img/anuncios/Anuncios.png"/>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <img class="text-center img-fluid" src="img/anuncios/Anuncios.png"/>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <img class="text-center img-fluid" src="img/anuncios/Anuncios.png"/>
                    </div>
                    <!------------------------Div para los carousel-------------------->
                    <div class="col-8 container-fluid border-right border-warning">

                        <div class="col-12">
                            <h3 class="font-weight-bold text-center" style="color: #4484CE">Tús series favoritas</h3>
                            <div class="carousel text-center">
                                <a class="carousel-item" href="#">
                                    <img id="imagenGOT" class="img-fluid" src="img/portadas/goty.jpg">
                                    <h6>Juego de Tronos</h6>
                                </a>
                                <a class="carousel-item" href="#">
                                    <img class="img-fluid" src="img/portadas/twd.png">
                                    <h6>The Walking Dead</h6>
                                </a>
                                <a class="carousel-item" href="#">
                                    <img class="img-fluid" src="img/portadas/lcdp.png">
                                    <h6>La Casa De Papel</h6>
                                </a>
                                <a class="carousel-item" href="#">
                                    <img class="img-fluid" src="img/portadas/lost.png">
                                    <h6>Lost</h6>
                                </a>
                                <a class="carousel-item" href="#">
                                    <img class="img-fluid" src="img/portadas/vikings.jpg">
                                    <h6>Vikingos</h6>
                                </a>
                                <a class="carousel-item" href="#">
                                    <img class="img-fluid" src="img/portadas/elite.jpg">
                                    <h6>Élite</h6>
                                </a>
                            </div>

                        </div>

                        <div class="col-12 ">
                            <br>
                            <h3 class="font-weight-bold text-center" style="color: #4484CE">Tús series Españolas</h3>
                            <div class="carousel text-center">
                                <a class="carousel-item" href="#">
                                    <img class="img-fluid" src="img/portadas/elite.jpg">
                                    <h6>Élite</h6>
                                </a>
                                <a class="carousel-item" href="#">
                                    <img class="img-fluid" src="img/portadas/lcdp.png">
                                    <h6>La Casa De Papel</h6>
                                </a>
                                <a class="carousel-item" href="#">
                                    <img class="img-fluid" src="img/portadas/elite.jpg">
                                    <h6>Élite</h6>
                                </a>
                                <a class="carousel-item" href="#">
                                    <img class="img-fluid" src="img/portadas/lcdp.png">
                                    <h6>La Casa De Papel</h6>
                                </a>
                            </div>
                        </div>

                        <div class="col-12>"
                            <br>
                            <h3 class="font-weight-bold text-center" style="color: #4484CE">Tús series en trending</h3>
                            <div class="carousel text-center">
                                <a class="carousel-item" href="#">
                                    <img class="img-fluid" id="imagenGOT1" src="img/portadas/goty.jpg">
                                    <h6>Juego de Tronos</h6>
                                </a>
                                <a class="carousel-item" href="#">
                                    <img class="img-fluid" src="img/portadas/lcdp.png">
                                    <h6>La Casa De Papel</h6>
                                </a>
                                <a class="carousel-item" href="#">
                                    <img class="img-fluid" src="img/portadas/twd.png">
                                    <h6>The Walking Dead</h6>
                                </a>
                                <a class="carousel-item" href="#">
                                    <img class="img-fluid" src="img/portadas/elite.jpg">
                                    <h6>Élite</h6>
                                </a>
                                <a class="carousel-item" href="#">
                                    <img class="img-fluid" src="img/portadas/vikings.jpg">
                                    <h6>Vikingos</h6>
                                </a>
                            </div>
                        </div> 
                    </div>        
                    <!----------------------------------------------------------------->


                    <div class="col-2" >
                        <div class="row">
                            <!--------------------Título del feed---------------------->
                            <div class="col-12">
                                <h3 class="text-center font-weight-bold text-amarillo" style="color: #4484CE">Top series mejor valoradas</h3>
                            </div>
                            <div class="col-12"><br></div>
                            <!------------------------Serie 1-------------------------->
                            <div class="col-4">
                                <br>
                                <h5 class="text-center font-weight-bold">1º</h5>
                            </div>
                            <div class="col-8">
                                <img id="imagenGOT2" class="img-fluid" src="img/portadasfeed/goty.jpg">
                            </div>
                            <div class="col-12"><br></div>
                            <!------------------------Serie 2-------------------------->
                            <div class="col-4">
                                <br>
                                <h5 class="text-center font-weight-bold">2º</h5>
                            </div>
                            <div class="col-8">
                                <img class="img-fluid" src="img/portadasfeed/twd.png">
                            </div>
                            <div class="col-12"><br></div>
                            <!------------------------Serie 3-------------------------->
                            <div class="col-4">
                                <br>
                                <h5 class="text-center font-weight-bold">3º</h5>
                            </div>
                            <div class="col-8">
                                <img class="img-fluid" src="img/portadasfeed/lcdp.png">
                            </div>
                            <div class="col-12"><br></div>
                            <!------------------------Serie 4-------------------------->
                            <div class="col-4">
                                <br>
                                <h5 class="text-center font-weight-bold">4º</h5>
                            </div>
                            <div class="col-8">
                                <img class="img-fluid" src="img/portadasfeed/vikings.jpg">
                            </div>
                            <div class="col-12"><br></div>
                            <!------------------------Serie 5-------------------------->
                            <div class="col-4">
                                <br>
                                <h5 class="text-center font-weight-bold">5º</h5>
                            </div>

                            <div class="col-8">
                                <img class="img-fluid" src="img/portadasfeed/elite.jpg">
                            </div>
                            <div class="col-12"><br></div>        
                            
                            <!--------------------Título del feed---------------------->
                            <div class="col-12">
                                <br><h3 class="text-center font-weight-bold" style="color: #4484CE">Top series Españolas</h3>
                            </div>
                            <div class="col-12"><br></div>
                            <!------------------------Serie 1-------------------------->
                            <div class="col-4">
                                <br>
                                <h5 class="text-center font-weight-bold">1º</h5>
                            </div>
                            <div class="col-8">
                                <img class="img-fluid" src="img/portadasfeed/lcdp.png">
                            </div>
                            <div class="col-12"><br></div>
                            <!------------------------Serie 2-------------------------->
                            <div class="col-4">
                                <br>
                                <h5 class="text-center font-weight-bold">2º</h5>
                            </div>
                            <div class="col-8">
                                <img class="img-fluid" src="img/portadasfeed/elite.jpg">
                            </div>
                            <div class="col-12"><br></div>
                        </div>       
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

<script>
    $('.carousel').carousel();
    $('.carousel').carousel().numVisible = 5;
    console.log('lanzo inicioUserDesign');
</script>

<script>
    $('#imagenGOT').click(function () {
        $('#principal').load('serieDesign.php');
        console.log('lanzo serieDesign');
    });
    $('#imagenGOT1').click(function () {
        $('#principal').load('serieDesign.php');
        console.log('lanzo serieDesign');
    });
    $('#imagenGOT2').click(function () {
        $('#principal').load('serieDesign.php');
        console.log('lanzo serieDesign');
    });
</script> 




